/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
/* compiler stuff */
#include <stdarg.h>
#include <stdbool.h>
/*----------------------------------------------------------------------------*/
/* ulinux stuff */
#include <ulinux/compiler_types.h>
#include <ulinux/compiler_misc.h>
#include <ulinux/types.h>
#include <ulinux/sysc.h>

#include <ulinux/file.h>
#include <ulinux/socket/socket.h>
#ifdef CONFIG_IPV4
#include <ulinux/socket/in.h>
#else
#include <ulinux/socket/in6.h>
#endif
#include <ulinux/signal/signal.h>
#include <ulinux/error.h>
#include <ulinux/epoll.h>
#include <ulinux/utils/mem.h>
#include <ulinux/utils/endian.h>
#include <ulinux/mmap.h>
#include <ulinux/time.h>
#include <ulinux/select.h>
#include <ulinux/stat.h>
#include <ulinux/utsname.h>

#include <ulinux/utils/ascii/ascii.h>
#include <ulinux/utils/ascii/string/string.h>
#include <ulinux/utils/ascii/string/vsprintf.h>

#include "ulinux_namespace.h"
#include "exit_codes.h"
/*----------------------------------------------------------------------------*/
/* trace/debug/whatever stuff */
#if 0
#define BUFSIZ 8192
static u8 dprint_buf[BUFSIZ];
#define PERR(fmt,...) ulinux_dprintf(2, &dprint_buf[0], BUFSIZ, fmt, ##__VA_ARGS__)
#endif
/*----------------------------------------------------------------------------*/
#ifdef CONFIG_IPV4
/* 32 bits value for the IPv4 address, can be INADDR_ANY */
#define LISTENING_IPV4 INADDR_ANY
#else
static struct ulinux_in6_addr listening_ipv6;
#endif

/*
 * time out for a socket read/write, in seconds. 4 secs is huge
 * TODO: don't think it is a good idea, look at tcp code from syncsm
 */
#define CNX_WAIT_TIMEOUT 4

/* 5 minutes */
#define SMTP_SERVER_TIMEOUT (5*60)
/*----------------------------------------------------------------------------*/
#define SMTP_CMD_RESP_LINE_LEN_MAX 512 	/* rfc 5321 limits */
/* don't forget any starting '.' for smtp transparency */
#define SMTP_TXT_LINE_LEN_MAX 1001 	/* rfc 5321 limits */

#define SMTP_GREETING_FMT "220 %s lnanosmtp ready\r\n"
/* XXX: don't forget to check if we can announce 8 bits clean transport */
#define SMTP_HELLO_RESP_FMT "250 %s\r\n"
#define SMTP_MAIL_RESP_OK "250 OK\r\n"
#define SMTP_RCPT_RESP_NOK "550 something wrong related to the local part\r\n"
#define SMTP_DATA_RESP_OK "354 start mail input; end with <crlf>.<crlf>\r\n"
#define SMTP_DATA_RCV_OK "250 OK\r\n"
#define SMTP_QUIT_RESP "221 closing transmission channel\r\n"

#define SIGBIT(sig) (1 << (sig - 1))
/*----------------------------------------------------------------------------*/
/* sockets stuff */
#ifdef CONFIG_IPV4
static struct sockaddr_in srv_addr;
#else
static struct sockaddr_in6 srv_addr;
#endif
static si srv_sock;	/* the main listening socket */
static si cnx_sock;	/* a cnx socket from accept */

static ul cnx_sock_fd_set[FD_SET_ULS_N];
static u8 cnx_sock_fd_set_ul_idx;
static u8 cnx_sock_fd_ul_shift;
/*----------------------------------------------------------------------------*/
/* fd facilities */
static si sigs_fd;	/* fd for signals */
static si epfd;		/* epoll fd */
/*----------------------------------------------------------------------------*/
/* the line buffer */
static u8 cmd_resp_line[SMTP_CMD_RESP_LINE_LEN_MAX];
/* the following is a ul because mainly linux syscall argument */
static ul cmd_line_bytes_rd_n;	/* keep an eye on how much was read */
/* points on '\r' of the line terminating "\r\n" */
static u8 *cmd_line_end;
/*----------------------------------------------------------------------------*/
/* RCPT TO:<forward_path> */
#define FORWARD_PATH_START_IDX 9
static u8 *forward_path_start;
static u8 *forward_path_end;	/* points on the terminating '>' */
static u8 *local_part_end;	/* points on the terminating  '@' or '>'*/
#define SMTP_RCPTS_N_MAX 100	/* rfc 5321 limits */
#define SMTP_RCPT_LEN_MAX 64	/* rfc 5321 limits */
static u8 smtp_rcpts_n;
struct smtp_rcpt {
	u8 local_part[SMTP_RCPT_LEN_MAX + 1];
	si fd;
};
static struct smtp_rcpt smtp_rcpts[SMTP_RCPTS_N_MAX];
/*----------------------------------------------------------------------------*/
/*
 * the following is the plumbering for the uniq maildir file, see maildir
 * specs: http://cr.yp.to/proto/maildir.html
 */
/*
 * Each hostname char can be expanded to 4 chars because of the path chars:
 *   '/' -> "\057"
 *   ':' -> "\072"
 * On linux, we use the nodename from the uname syscall as a hostname
 */
#define HOSTNAME_LEN_MAX (UTS_LEN * 4)

#if BITS_PER_LONG==64
	#define LONG_DEC_STR_MAX "4294967295"
#elif BITS_PER_LONG==32
	#define LONG_DEC_STR_MAX "18446744073709551615"
#else
	#error "architecture long type size is not supported 8-|"
#endif

#define CSTRLEN(cstr) (sizeof(cstr) - 1)
/* seconds.Mmicroseconds.hostname\0 */
#define UNIQ_FMT "%d.M%d.%s"
#define UNIQ_LEN_MAX					\
	CSTRLEN(LONG_DEC_STR_MAX) + CSTRLEN(".M")	\
	+ CSTRLEN(LONG_DEC_STR_MAX) + CSTRLEN(".")	\
	+ HOSTNAME_LEN_MAX
static u8 uniq[UNIQ_LEN_MAX + 1];

/* local_part/[tmp|new]\0 */
#define MAILDIR_TMP_DIR_FMT "/" "%s" "/tmp"
#define MAILDIR_NEW_DIR_FMT "/" "%s" "/new"
#define MAILDIR_DIR_LEN_MAX (1 + SMTP_RCPT_LEN_MAX + 4)
static u8 maildir_dir[MAILDIR_DIR_LEN_MAX + 1];

#define MAILDIR_TMP_PATH_FMT MAILDIR_TMP_DIR_FMT "/" "%s"
#define MAILDIR_NEW_PATH_FMT MAILDIR_NEW_DIR_FMT "/" "%s"
#define MAILDIR_PATH_LEN_MAX (MAILDIR_DIR_LEN_MAX + 1 + UNIQ_LEN_MAX)
static u8 maildir_tmp_path[MAILDIR_PATH_LEN_MAX + 1];
static u8 maildir_new_path[MAILDIR_PATH_LEN_MAX + 1];
/*----------------------------------------------------------------------------*/
static u8 txt_line[SMTP_TXT_LINE_LEN_MAX];
static u8 *txt_line_end;	/* points right after the last txt line byte*/
static u8 *txt_line_rd_start;
static u8 *txt_line_rd_end;	/* points right after the last rd byte */
static u8 *terminator;		/* points on the '\n' of the terminator */
/*----------------------------------------------------------------------------*/
/* some shared return values */
#define SUCCESS			0

#define SMTP_CLOSE		1
#define SMTP_NOT_RCPT		2
#define SMTP_END_OF_DATA	3
#define SMTP_TXT_LINE_RD_MORE	4
#define SMTP_NOT_MAIL		5

#define FALSE	0
#define TRUE	1
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
static void epoll_srv_sock_setup(void)
{
	struct epoll_event ep_evt;
	sl r;

	memset(&ep_evt, 0, sizeof(ep_evt));
	ep_evt.events = EPOLLIN | EPOLLPRI;
	ep_evt.data.fd = srv_sock;

	r = epoll_ctl(epfd, EPOLL_CTL_ADD, srv_sock, &ep_evt);
	if (ISERR(r))
		exit(SRV_SOCK_SETUP_EPOLL_CTL_ADD_FAILURE);
}

/* if we have an error or we time out, notify for socket closing */
#define CNX_SOCK_SEND_WAIT_FAILURE 1
#define fd_set cnx_sock_fd_set
#define ul_idx cnx_sock_fd_set_ul_idx
#define ul_shift cnx_sock_fd_ul_shift
static u8 cnx_sock_send_wait(void)
{
	struct timespec ts;
	sl r;

	memset(&fd_set[0], 0, sizeof(fd_set));
	fd_set[ul_idx] = (1UL << ul_shift);

	ts.sec = SMTP_SERVER_TIMEOUT;
	ts.nsec = 0;

	loop {	
		/* pselect6 is common to aarch64 and x86_64 */
		r = pselect6(cnx_sock + 1, 0, &fd_set[0], 0, &ts, 0);
		/*
		 * XXX: to address some concerns: by design for the sake of
		 * simplicity we ignore handled signals here. May do "right(tm)"
		 * state management around epoll_pwait someday.
		 */
		if (r != -EINTR)
			break;
	}

	if (ISERR(r) || r == 0) /* r != 1, because only 1 fd used in fd sets */
		return CNX_SOCK_SEND_WAIT_FAILURE;
	return SUCCESS;
}
#undef fd_set
#undef byte_offset
#undef byte_shift

#define CNX_SOCK_SEND_LINE_FAILURE -1 
static sl cnx_sock_send_line(ul offset, u64 sz)
{
	sl bytes_sent_n;

	loop {
		/*
		 * XXX:does write ensure the data is received by the other
		 * side?
		 * If so, it needs a timeout to be set somewhere: socket
		 * options?
		 * This is TCP, not UDP.
		 */
		bytes_sent_n = write(cnx_sock, &cmd_resp_line[0] + offset, sz);
		if (bytes_sent_n != -EINTR) {
			if (ISERR(bytes_sent_n))
				bytes_sent_n = CNX_SOCK_SEND_LINE_FAILURE;
			break;
		}
	}
	return bytes_sent_n;
}

/* if we have an error or we time out, notify for socket closing */
#define CNX_SOCK_RD_WAIT_FAILURE 1
#define fd_set cnx_sock_fd_set
#define ul_idx cnx_sock_fd_set_ul_idx
#define ul_shift cnx_sock_fd_ul_shift
static u8 cnx_sock_rd_wait(void)
{
	struct timespec ts;
	sl r;

	memset(&fd_set[0], 0, sizeof(fd_set));
	fd_set[ul_idx] = (1UL << ul_shift);

	ts.sec = SMTP_SERVER_TIMEOUT;
	ts.nsec = 0;

	loop {
		/* pselect6 is common to aarch64 and x86_64 */
		r = pselect6(cnx_sock + 1, &fd_set[0], 0, 0, &ts, 0);
		/*
		 * XXX: to address some concerns: by design for the sake of
		 * simplicity we ignore handled signals here. May do "right(tm)"
		 * state management around epoll_pwait someday
		 */
		if (r != -EINTR)
			break;
	}

	if (ISERR(r) || r == 0) /* r != 1 because only 1 fd used in fd sets */
		return CNX_SOCK_RD_WAIT_FAILURE;
	return SUCCESS;
}
#undef fd_set
#undef byte_offset
#undef byte_shift

#define CNX_SOCK_RD_FAILURE -1 
static sl cnx_sock_cmd_line_rd(void) 
{
	sl bytes_rd_n;

	loop {
		/* enforce max line size from protocol */
		bytes_rd_n = read(cnx_sock, &cmd_resp_line[0]
			+ cmd_line_bytes_rd_n, SMTP_CMD_RESP_LINE_LEN_MAX
							- cmd_line_bytes_rd_n);
		if (bytes_rd_n != -EINTR) {
			if (ISERR(bytes_rd_n))
				bytes_rd_n = CNX_SOCK_RD_FAILURE;
			break;
		}
	}
	return bytes_rd_n;
}

static u8 maildir_rcpts_links_move_from_tmp_to_new(void)
{
	u8 r0;
	u8 rcpt;

	rcpt = 0;

	r0 = SUCCESS;

	loop {
		sl r1;

		if (rcpt == smtp_rcpts_n)
			break;

		maildir_tmp_path[MAILDIR_PATH_LEN_MAX] = 0;
		snprintf(&maildir_tmp_path[0], MAILDIR_PATH_LEN_MAX,
				MAILDIR_TMP_PATH_FMT,
				&smtp_rcpts[rcpt].local_part[0], &uniq[0]);

		maildir_new_path[MAILDIR_PATH_LEN_MAX] = 0;
		snprintf(&maildir_new_path[0], MAILDIR_PATH_LEN_MAX,
				MAILDIR_NEW_PATH_FMT,
				&smtp_rcpts[rcpt].local_part[0], &uniq[0]);

		/* linkat is common to aarch64 and x86_64 */
		r1 = linkat(&maildir_tmp_path[0], &maildir_new_path[0]);
		if (ISERR(r1)) {
			r0 = SMTP_CLOSE;
			break;
		}
		/* unlinkat is common to aarch64 and x86_64 */
		(void)unlinkat(&maildir_tmp_path[0]);

		++rcpt;
	}
	return r0;
}

static void maildir_rcpts_files_close(void)
{
	u8 rcpt;

	rcpt = 0;

	loop {
		if (rcpt == smtp_rcpts_n)
			break;

		close(smtp_rcpts[rcpt].fd);

		++rcpt;
	}
}

static void maildir_rcpts_files_sync(void)
{
	u8 rcpt;

	rcpt = 0;

	loop {
		if (rcpt == smtp_rcpts_n)
			break;

		fsync(smtp_rcpts[rcpt].fd);

		++rcpt;
	}
}

static void maildir_rcpts_tmp_links_remove(void)
{
	u8 rcpt;

	rcpt = 0;

	loop {
		if (rcpt == smtp_rcpts_n)
			break;

		maildir_tmp_path[MAILDIR_PATH_LEN_MAX] = 0;
		snprintf(&maildir_tmp_path[0], MAILDIR_PATH_LEN_MAX,
				MAILDIR_TMP_PATH_FMT,
				&smtp_rcpts[rcpt].local_part[0], &uniq[0]);
		/* unlinkat is common to aarch64 and x86_64 */
		(void)unlinkat(&maildir_tmp_path[0]);

		++rcpt;
	}
}

static void maildir_rcpts_new_links_remove(void)
{
	u8 rcpt;

	rcpt = 0;

	loop {
		if (rcpt == smtp_rcpts_n)
			break;

		maildir_tmp_path[MAILDIR_PATH_LEN_MAX] = 0;
		snprintf(&maildir_new_path[0], MAILDIR_PATH_LEN_MAX,
				MAILDIR_NEW_PATH_FMT,
				&smtp_rcpts[rcpt].local_part[0], &uniq[0]);
		/* unlinkat is common to aarch64 and x86_64 */
		(void)unlinkat(&maildir_new_path[0]);

		++rcpt;
	}
}

#define MAILDIR_DIRS_OK			0
#define MAILDIR_DIRS_NOK		1
static u8 maildir_dir_validate(void)
{
	/*
	 * TODO: will have to be "upgraded" to statx once the kernels are
	 * "recent enough", For now we use newfstatat because it's common
	 * to x86_64 and aarch64 archs.
	 */
	struct sstat dir_stat;
	sl r;

	memset(&dir_stat, 0, sizeof(dir_stat));

	r = newfstatat(&maildir_dir[0], &dir_stat);
	if (ISERR(r) || ((dir_stat.mode & S_IFDIR) == 0))
		return MAILDIR_DIRS_NOK;
	return MAILDIR_DIRS_OK;
}

static u8 maildir_dirs_validate(void)
{
	sl r;

	/* XXX:keep this in mind! */
	*local_part_end = 0;

	maildir_dir[MAILDIR_DIR_LEN_MAX] = 0;
	snprintf(&maildir_dir[0], MAILDIR_DIR_LEN_MAX, MAILDIR_TMP_DIR_FMT,
							forward_path_start);

	r = maildir_dir_validate();
	if (r == MAILDIR_DIRS_NOK)
		goto exit;

	maildir_dir[MAILDIR_DIR_LEN_MAX] = 0;
	snprintf(&maildir_dir[0], MAILDIR_DIR_LEN_MAX, MAILDIR_NEW_DIR_FMT,
							forward_path_start);
	r = maildir_dir_validate();
exit:	
	return r;
}

static u8 maildir_rcpts_tmp_files_create(void)
{
	u8 r0;
	u8 rcpt;

	rcpt = 0;
	r0 = SUCCESS;
	
	loop {
		sl r1;

		if (rcpt == smtp_rcpts_n)
			break;

		maildir_tmp_path[MAILDIR_PATH_LEN_MAX] = 0;
		snprintf(&maildir_tmp_path[0], MAILDIR_PATH_LEN_MAX,
				MAILDIR_TMP_PATH_FMT,
				&smtp_rcpts[rcpt].local_part[0], &uniq[0]);
		/*
		 * carefull: open mode is subjet to umask masking.
		 * openat is common to aarch64 and x86_64
		 */
		r1 = openat(&maildir_tmp_path, O_WRONLY | O_CREAT | O_TRUNC,
				S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH
								| S_IWOTH);
		if (ISERR(r1)) {
			r0 = SMTP_CLOSE;
			break;
		}
		smtp_rcpts[rcpt].fd = (si)r1;
		++rcpt;
	}
	return r0;
}

static void nodename_pathize(u8 *uniq_hostname, u8 *nodename)
{
	loop {
		if (*nodename == 0) {
			*uniq_hostname = 0;
			break;
		}

		if (*nodename == '/') {
			uniq_hostname[0] = '\\';
			uniq_hostname[1] = '0';
			uniq_hostname[2] = '5';
			uniq_hostname[3] = '7';

			uniq_hostname += 4;
		} else if (*nodename == ':') {
			uniq_hostname[0] = '\\';
			uniq_hostname[1] = '0';
			uniq_hostname[2] = '7';
			uniq_hostname[3] = '2';

			uniq_hostname += 4;
		} else {
			*uniq_hostname = *nodename;
			++uniq_hostname;
		}
		++nodename;
	}
}

/* do not expect sysctl /proc/sys/kernel/hostname */
static void maildir_uniq_hostname_build(u8 *uniq_hostname)
{
	struct utsname uniq_utsname;
	sl r;

	r = uname(&uniq_utsname);
	if (!ISERR(r))
		nodename_pathize(uniq_hostname, &uniq_utsname.nodename[0]);
}

static void maildir_uniq_build(void)
{
	u8 uniq_hostname[HOSTNAME_LEN_MAX + 1];
	struct timeval uniq_timeval;
	sl r;

	uniq_hostname[0] = 0;

	r = gettimeofday(&uniq_timeval);
	if (ISERR(r)) {
		uniq_timeval.sec = 0;
		uniq_timeval.usec = 0;
	}

	maildir_uniq_hostname_build(&uniq_hostname[0]);

	uniq[UNIQ_LEN_MAX] = 0;
	snprintf(&uniq[0], UNIQ_LEN_MAX, UNIQ_FMT, uniq_timeval.sec,
					uniq_timeval.usec, &uniq_hostname[0]);
}

static u8 smtp_line_send(u64 line_sz)
{
	ul offset;	/* pointer size arithmetics */

	offset = 0;

	loop {
		u8 r;
		sl bytes_sent_n;

		r = cnx_sock_send_wait();
		if (r == CNX_SOCK_SEND_WAIT_FAILURE)
			return SMTP_CLOSE;

		bytes_sent_n = cnx_sock_send_line(offset, line_sz);
		if (bytes_sent_n == CNX_SOCK_SEND_LINE_FAILURE
							|| bytes_sent_n == 0)
			break;/*  0 means the connection was closed */

		line_sz -= (u64)bytes_sent_n;

		if (line_sz == 0)
			return SUCCESS;	/* the line was sent */

		offset += (ul)bytes_sent_n;
	}
	return SMTP_CLOSE;
}

static u8 smtp_greeting_send(void)
{
	u64 greeting_sz;

	greeting_sz = snprintf(&cmd_resp_line[0], SMTP_CMD_RESP_LINE_LEN_MAX,
			SMTP_GREETING_FMT, CONFIG_DOMAIN_ADDRESS_LITERAL);
	if (greeting_sz == 0)
		return SMTP_CLOSE;
	return smtp_line_send(greeting_sz);
}

static u8 smtp_cmd_line_rd(void)
{
	u8 *c;

	c = &cmd_resp_line[0];
	cmd_line_bytes_rd_n = 0;

	loop {
		u8 r;
		sl bytes_rd_n;

		r = cnx_sock_rd_wait();
		if (r == CNX_SOCK_RD_WAIT_FAILURE)
			break;

		bytes_rd_n = cnx_sock_cmd_line_rd();
		if (bytes_rd_n == CNX_SOCK_RD_FAILURE || bytes_rd_n == 0)
			break;

		cmd_line_bytes_rd_n += bytes_rd_n;

		loop {
			if (bytes_rd_n-- == 0)
				break;

			if (c[0] == '\n') 
				if (cmd_line_bytes_rd_n >= 2 && c[-1] == '\r') {
					cmd_line_end = c - 1;
					return SUCCESS;
				}

			++c;
		}

		/* protocol limit */
		if (cmd_line_bytes_rd_n == SMTP_CMD_RESP_LINE_LEN_MAX)
			break;
	}
	return SMTP_CLOSE;
}

static u8 smtp_hello_is_helo(void)
{
	if (cmd_line_bytes_rd_n < 4)
		return FALSE;

	if (		(cmd_resp_line[0] == 'H' || cmd_resp_line[0] == 'h')
		&&	(cmd_resp_line[1] == 'E' || cmd_resp_line[1] == 'e')
		&&	(cmd_resp_line[2] == 'L' || cmd_resp_line[2] == 'l')
		&&	(cmd_resp_line[3] == 'O' || cmd_resp_line[3] == 'o'))
		return TRUE;
	return FALSE;
}

static u8 smtp_hello_is_ehlo(void)
{
	if (cmd_line_bytes_rd_n < 4)
		return FALSE;

	if (		(cmd_resp_line[0] == 'E' || cmd_resp_line[0] == 'e')
		&&	(cmd_resp_line[1] == 'H' || cmd_resp_line[1] == 'h')
		&&	(cmd_resp_line[2] == 'L' || cmd_resp_line[2] == 'l')
		&&	(cmd_resp_line[3] == 'O' || cmd_resp_line[3] == 'o'))
		return TRUE;
	return FALSE;
}

static u8 smtp_hello_chk(void)
{
	if (smtp_hello_is_helo() || smtp_hello_is_ehlo())
		return SUCCESS;
	return SMTP_CLOSE;
}

static u8 smtp_mail_chk(void)
{
	if (cmd_line_bytes_rd_n < 4)
		return FALSE;

	if (		(cmd_resp_line[0] == 'M' || cmd_resp_line[0] == 'm')
		&&	(cmd_resp_line[1] == 'A' || cmd_resp_line[1] == 'a')
		&&	(cmd_resp_line[2] == 'I' || cmd_resp_line[2] == 'i')
		&&	(cmd_resp_line[3] == 'L' || cmd_resp_line[3] == 'l'))
		return TRUE;
	return FALSE;
}

static u8 smtp_rcpt_chk(void)
{
	if (cmd_line_bytes_rd_n < 4)
		return FALSE;

	if (		(cmd_resp_line[0] == 'R' || cmd_resp_line[0] == 'r')
		&&	(cmd_resp_line[1] == 'C' || cmd_resp_line[1] == 'c')
		&&	(cmd_resp_line[2] == 'P' || cmd_resp_line[2] == 'p')
		&&	(cmd_resp_line[3] == 'T' || cmd_resp_line[3] == 't'))
		return TRUE;
	return FALSE;
}

static u8 smtp_data_chk(void)
{
	if (cmd_line_bytes_rd_n < 4)
		return FALSE;

	if (		(cmd_resp_line[0] == 'D' || cmd_resp_line[0] == 'd')
		&&	(cmd_resp_line[1] == 'A' || cmd_resp_line[1] == 'a')
		&&	(cmd_resp_line[2] == 'T' || cmd_resp_line[2] == 't')
		&&	(cmd_resp_line[3] == 'A' || cmd_resp_line[3] == 'a'))
		return TRUE;
	return FALSE;
}

static u8 smtp_quit_chk(void)
{
	if (cmd_line_bytes_rd_n < 4)
		return FALSE;

	if (		(cmd_resp_line[0] == 'Q' || cmd_resp_line[0] == 'q')
		&&	(cmd_resp_line[1] == 'U' || cmd_resp_line[1] == 'u')
		&&	(cmd_resp_line[2] == 'I' || cmd_resp_line[2] == 'i')
		&&	(cmd_resp_line[3] == 'T' || cmd_resp_line[3] == 't'))
		return TRUE;
	return FALSE;
}

static u8 smtp_hello_rcv(void)
{
	u8 r;

	r = smtp_cmd_line_rd();
	if (r != SMTP_CLOSE)
		r = smtp_hello_chk();
	return r;
}

static u8 smtp_mail_rcv(void)
{
	u8 r;
	r = smtp_cmd_line_rd();
	if (r != SMTP_CLOSE) {
		if (smtp_mail_chk())
			r = SUCCESS;
		else
			r = SMTP_NOT_MAIL;
	}
	return r;
}

static u8 smtp_rcpt_ok_send(void)
{
	strncpy(&cmd_resp_line[0], SMTP_MAIL_RESP_OK,
						SMTP_CMD_RESP_LINE_LEN_MAX);
	return smtp_line_send(sizeof(SMTP_MAIL_RESP_OK) - 1);
}

static u8 smtp_rcpt_nok_send(void)
{
	strncpy(&cmd_resp_line[0], SMTP_RCPT_RESP_NOK,
						SMTP_CMD_RESP_LINE_LEN_MAX);
	return smtp_line_send(sizeof(SMTP_RCPT_RESP_NOK) - 1);
}

/*
 * XXX: local part with a quoted-string is not supported, namely a "quoted '>'"
 * will be mis-interpreted
 */
static u8 forward_path_end_locate(void)
{
	u8 *c;

	c = forward_path_start;

	loop {
		if (c >= cmd_line_end)
			return SMTP_CLOSE;

		if (*c == '>') {
			forward_path_end = c;
			break;
		}
		++c;
	}
	return SUCCESS;
}

static void local_part_end_locate(void)
{
	u8 *c;

	c = forward_path_start;	

	/*
	 * XXX:
	 * - forward path source route is not supported
	 * - quoted-string is not supported, namely a quoted-string with '@'
	 *   will be misinterpreted
	 * - "case-unsensitive postmaster" is not supported 
	 */
	loop {
		if (c == forward_path_end || *c == '@')
			break;
		++c;
	}
	local_part_end = c;
}

static u8 smtp_rcpt_process(void)
{
	u8 r;
	s64 local_part_sz;

	r = forward_path_end_locate();
	if (r == SMTP_CLOSE) /* it means we don't have an end in the line buf */
		goto exit;

	/* empty forward path not tolerated */
	if (forward_path_start == forward_path_end) {
		r = SMTP_CLOSE;
		goto exit;
	}

	local_part_end_locate();

	/* empty local part not tolerated */
	if (forward_path_start == local_part_end) {
		r = SMTP_CLOSE;
		goto exit;
	}

	/* local part too big */
	local_part_sz = local_part_end - forward_path_start;
	if (local_part_sz > SMTP_RCPT_LEN_MAX) { 
		r = SMTP_CLOSE;
		goto exit;
	}

	/* XXX: we don't deal with the domain name/address literal part */

	r = maildir_dirs_validate();
	switch(r){
	case MAILDIR_DIRS_OK:
		/* local part size was already checked, local_part_end is 0 */
		strcpy(&smtp_rcpts[smtp_rcpts_n].local_part[0],
							forward_path_start);
		smtp_rcpts[smtp_rcpts_n].fd = -1;
		++smtp_rcpts_n;

		r = smtp_rcpt_ok_send();
		if (r == SMTP_CLOSE)
			goto exit;
		break;
	case MAILDIR_DIRS_NOK:
		r = smtp_rcpt_nok_send();
		if (r == SMTP_CLOSE)
			goto exit;
		break;
	}

	r = SUCCESS;	
exit:
	return r;
}

static u8 smtp_rcpt_rcv(void)
{
	u8 r;
	r = smtp_cmd_line_rd();
	if (r != SMTP_CLOSE) {
		if (!smtp_rcpt_chk())
			r = SMTP_NOT_RCPT;
		else
			r = smtp_rcpt_process();
	}
	return r;
}

static u8 smtp_hello_resp_send(void)
{
	u64 hello_resp_sz;

	hello_resp_sz = snprintf(&cmd_resp_line[0], SMTP_CMD_RESP_LINE_LEN_MAX,
			SMTP_HELLO_RESP_FMT, CONFIG_DOMAIN_ADDRESS_LITERAL);

	if (hello_resp_sz == 0)
		return SMTP_CLOSE;
	return smtp_line_send(hello_resp_sz);
}

static u8 smtp_mail_resp_ok_send(void)
{
	strncpy(&cmd_resp_line[0], SMTP_MAIL_RESP_OK,
						SMTP_CMD_RESP_LINE_LEN_MAX);
	return smtp_line_send(sizeof(SMTP_MAIL_RESP_OK) - 1);
}

static u8 smtp_rcpts_load(void)
{
	u8 r;

	smtp_rcpts_n = 0;

	loop {
		r = smtp_rcpt_rcv();
		if (r != SUCCESS)
			break;
	}
	if (r == SMTP_NOT_RCPT)
		r = SUCCESS;
	return r;
}

static u8 smtp_data_resp_ok_send(void)
{
	strncpy(&cmd_resp_line[0], SMTP_DATA_RESP_OK,
						SMTP_CMD_RESP_LINE_LEN_MAX);
	return smtp_line_send(sizeof(SMTP_DATA_RESP_OK) - 1);
}

static u8 smtp_data_rcv_ok_send(void)
{
	strncpy(&cmd_resp_line[0], SMTP_DATA_RCV_OK,
						SMTP_CMD_RESP_LINE_LEN_MAX);
	return smtp_line_send(sizeof(SMTP_DATA_RCV_OK) - 1);
}

static u8 smtp_quit_resp_send(void)
{
	strncpy(&cmd_resp_line[0], SMTP_QUIT_RESP, SMTP_CMD_RESP_LINE_LEN_MAX);
	return smtp_line_send(sizeof(SMTP_QUIT_RESP) - 1);
}

static sl cnx_sock_txt_line_rd(void) 
{
	sl bytes_rd_n;

	loop {
		ul bytes_to_rd_n;	/* pointer size arithmetics */

		bytes_to_rd_n = txt_line_end - txt_line_rd_start;

		bytes_rd_n = read(cnx_sock, txt_line_rd_start, bytes_to_rd_n);
		if (bytes_rd_n != -EINTR) {
			if (ISERR(bytes_rd_n))
				bytes_rd_n = CNX_SOCK_RD_FAILURE;
			break;
		}
	}
	return bytes_rd_n;
}

static u8 rcpt_write(si fd, ul sz)
{
	u8 r0;
	ul bytes_written_n;
	u8 *txt_line_start;

	bytes_written_n = 0;

	/*====================================================================*/
	/* smtp transparency: allow sending lines with only '.' */
	if (txt_line[0] == '.') {
		txt_line_start = &txt_line[1];
		sz--;
	} else
		txt_line_start = &txt_line[0];
	/*====================================================================*/

	loop {
		sl r1;

		r1 = write(fd, txt_line_start + bytes_written_n, sz
							- bytes_written_n);

		if (r1 == -EINTR)
			continue;

		if (ISERR(r1)) {
			r0 = SMTP_CLOSE;
			break;
		}

		bytes_written_n += (ul)r1;

		if (sz == bytes_written_n) {
			r0 = SUCCESS;
			break;
		}
	}
	return r0;
}

static u8 txt_line_rcpts_write(void)
{
	u8 rcpt;
	u8 r;

	rcpt = 0;

	loop {
		ul txt_line_sz;	/* pointer size arithmetics */

		if (rcpt == smtp_rcpts_n) {
			r = SUCCESS;
			break;
		}

		txt_line_sz = terminator - &txt_line[0] + 1;

		r = rcpt_write(smtp_rcpts[rcpt].fd, txt_line_sz);
		if (r == SMTP_CLOSE)
			break;
			
		++rcpt;
	}
	return r;
}

static u8 terminator_is_end_of_data(void)
{
	if ((terminator == &txt_line[2]) && (terminator[-2] == '.'))
		return TRUE;
	return FALSE;
}

/* only explore from txt_line_rd_start, to txt_line_rd_end or txt_line_end */
#define TERMINATOR_NOT_IN_RD_BYTES	0
#define TERMINATOR_TXT_LINE_END_REACHED	1
#define TERMINATOR_FOUND		2
static u8 terminator_lookup(void)
{
	u8 r;
	u8 *c;

	c = txt_line_rd_start;
	
	loop {
		if ((c[0] == '\n') && (c >= &txt_line[1]) && (c[-1] == '\r')) {
			terminator = c;
			r = TERMINATOR_FOUND;
			break;
		}

		++c;

		if (c == txt_line_end) {
			r = TERMINATOR_TXT_LINE_END_REACHED;
			break;
		}

		if (c == txt_line_rd_end) {
			r = TERMINATOR_NOT_IN_RD_BYTES;
			break;
		}
	}
	return r;
}

static void txt_line_shift(void)
{
	ul txt_line_cpy_sz; 	/* pointer size arithmetics */

	txt_line_cpy_sz = txt_line_rd_end - (terminator + 1);
	memcpy(&txt_line[0], terminator + 1, txt_line_cpy_sz);

	txt_line_rd_start = &txt_line[0];
	txt_line_rd_end =  &txt_line[0] + txt_line_cpy_sz;
}

static u8 txt_line_rd_bytes_process(void)
{
	u8 r;

	loop {
		r = terminator_lookup();

		if (r == TERMINATOR_NOT_IN_RD_BYTES) {
			txt_line_rd_start = txt_line_rd_end;
			r = SMTP_TXT_LINE_RD_MORE;
			break;
		} else if (r == TERMINATOR_TXT_LINE_END_REACHED) {
			r = SMTP_CLOSE; /* no terminator in rfc limits */
			break;
		} 

		/* TERMINATOR_FOUND */
		
		if (terminator_is_end_of_data()) {
			r = SMTP_END_OF_DATA;
			break;
		}

		r = txt_line_rcpts_write();
		if (r == SMTP_CLOSE)
			break;

		/* if we have more rd bytes, shift them and start again, else need more
		   bytes */
		if ((terminator + 1) == txt_line_rd_end) {
			txt_line_rd_start = &txt_line[0];
			r = SMTP_TXT_LINE_RD_MORE;
			break;
		} else 
			txt_line_shift();
	}
	return r;
}

static u8 smtp_data_rcv(void)
{
	u8 r;

	txt_line_rd_start = &txt_line[0];

	r = SUCCESS;

	loop {
		sl bytes_rd_n;

		r = cnx_sock_rd_wait();
		if (r == CNX_SOCK_RD_WAIT_FAILURE) {
			r = SMTP_CLOSE;
			break;
		}

		bytes_rd_n = cnx_sock_txt_line_rd();
		if (bytes_rd_n == CNX_SOCK_RD_FAILURE || bytes_rd_n == 0) {
			r = SMTP_CLOSE;
			break;
		}

		txt_line_rd_end = txt_line_rd_start + bytes_rd_n;

		r = txt_line_rd_bytes_process();
		if (r == SMTP_CLOSE)
			break;
		else if (r == SMTP_END_OF_DATA) {
			r = SUCCESS;
			break;
		}

		/* SMTP_TXT_LINE_RD_MORE */
	}
	return r;
}

static u8 smtp_mail_transactions(void)
{
	u8 r;

	loop {
		/* could be SMTP_CLOSE or SMTP_NOT_MAIL */
		r = smtp_mail_rcv();	
		if (r != SUCCESS)
			break;

		r = smtp_mail_resp_ok_send();
		if (r == SMTP_CLOSE)
			break;

		r = smtp_rcpts_load();
		if (r == SMTP_CLOSE)
			break;

		/*
		 * XXX: from here we have the client next command line already
		 *      loaded
		 */

		if (smtp_rcpts_n == 0) {
			r = SMTP_CLOSE;
			break;
		}

		if (!smtp_data_chk()) {
			r = SMTP_CLOSE;
			break;
		}

		r = smtp_data_resp_ok_send();
		if (r == SMTP_CLOSE)
			break;	

		maildir_uniq_build();

		r = maildir_rcpts_tmp_files_create();
		if (r == SMTP_CLOSE) 
			goto err_remove_tmp_files;

		r = smtp_data_rcv();
		if (r == SMTP_CLOSE)
			goto err_remove_tmp_files;

		r = maildir_rcpts_links_move_from_tmp_to_new();
		if (r == SMTP_CLOSE)
			goto err_remove_tmp_and_new_files;

		maildir_rcpts_files_sync();
		maildir_rcpts_files_close();

		r = smtp_data_rcv_ok_send();
		if (r == SMTP_CLOSE)
			break;
	}
	return r;

err_remove_tmp_and_new_files:
	maildir_rcpts_new_links_remove();
err_remove_tmp_files:
	maildir_rcpts_tmp_links_remove();
	maildir_rcpts_files_close();
	return SMTP_CLOSE;
}

static u8 smtp_prolog(void)
{
	u8 r;

	r = smtp_greeting_send();
	if (r == SMTP_CLOSE)
		goto exit;

	r = smtp_hello_rcv();
	if (r == SMTP_CLOSE)
		goto exit;

	r = smtp_hello_resp_send();
	if (r == SMTP_CLOSE)
		goto exit;
exit:
	return r;
}

static void cnx_handle(void)
{
	u8 r;

	cmd_line_bytes_rd_n = 0;

	r = smtp_prolog();
	if (r == SMTP_CLOSE)
		goto exit;

	r = smtp_mail_transactions();
	if (r == SMTP_NOT_MAIL && smtp_quit_chk())
		smtp_quit_resp_send();
exit:
	return;
}

static void cnx_sock_close(void)
{
	loop {
		sl r;

  		r = close(cnx_sock);
		if (r != -EINTR) /* ignores errors */
			break;
	}
}

static void cnx_sock_fd_set_params(void)
{
	si ul_bits_n;

	ul_bits_n = 8 * sizeof(ul);

	cnx_sock_fd_set_ul_idx = cnx_sock / ul_bits_n;
	cnx_sock_fd_ul_shift = cnx_sock % ul_bits_n;
}

static void cnxs_consume(void)
{
	loop {
		sl r;
#ifdef CONFIG_IPV4
		struct sockaddr_in peer;
#else
		struct sockaddr_in6 peer;
#endif
		sl peer_len;

		peer_len = sizeof(peer);
		loop {
			r = accept(srv_sock, &peer, &peer_len);
			/* based on man page */
			if (r != -EINTR && r != ECONNABORTED) 
				break;
			/* SIGSTOP will generate a EINTR */
		}
		
		if (r != -EAGAIN && ISERR(r))
			exit(CNXS_CONSUME_ACCEPT_GENERIC_FAILURE);
		if (peer_len != sizeof(peer))
			exit(CNXS_CONSUME_ACCEPT_WRONG_PEER);
		if (r == -EAGAIN)
			break;	/* no more connexion pending */
		
		cnx_sock=(si)r;

		cnx_sock_fd_set_params();

		cnx_handle();
		cnx_sock_close();
	}
}

static void sigs_consume(void)
{
	struct signalfd_siginfo info;
	
	loop {
		sl r;

		loop {
			memset(&info, 0, sizeof(info));
			r = read(sigs_fd, &info, sizeof(info));
			if (r != -EINTR)
				break;
		}
		if (r != -EAGAIN && ((ISERR(r) || (r > 0
							&& r != sizeof(info)))))
			exit(SIGS_CONSUME_SIGINFO_READ_FAILURE);
		if (r == 0 || r == -EAGAIN)
			break;
	
		switch (info.ssi_signo) {
		case SIGTERM:
			exit(0);
			break;
		/* please, do add the ones you like */
		}
	}
}

static void main_loop(void)
{
	loop {
		struct epoll_event evts[2];	/*sigs_fd and srv_sock */
		sl r;
		sl j;
	
		loop {
			memset(evts, 0, sizeof(evts));
			/* epoll_pwait is common to aarch64 and x86_64 */
			r = epoll_pwait(epfd, evts, 2, -1);
			if (r != -EINTR)
				break;
		}
		if (ISERR(r))
			exit(MAIN_LOOP_EPOLL_WAIT_GENERIC_FAILURE);

		j = 0;
		loop {
			if (j == r)
				break;

			if (evts[j].data.fd == sigs_fd) {
			  if(evts[j].events & EPOLLIN)
				sigs_consume();
			  else
				exit(MAIN_LOOP_EPOLL_WAIT_SIGS_FD_EVENT_IS_NOT_EPOLLIN);
			} else if (evts[j].data.fd == srv_sock) {
			  if (evts[j].events & (EPOLLERR | EPOLLHUP | EPOLLPRI))
				exit(MAIN_LOOP_EPOLL_WAIT_SRV_SOCK_UNEXPECTED_EVENT);
			  else if (evts[j].events & EPOLLIN)
				cnxs_consume();
			  else
				exit(MAIN_LOOP_EPOLL_WAIT_SRV_SOCK_UNKNOWN_FAILURE);
			}

			++j;
		}
	}
}

static void srv_sock_create(void)
{
	sl bool_true;
	sl r;

#ifdef CONFIG_IPV4
	r = socket(AF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
#else
	r = socket(AF_INET6, SOCK_STREAM | SOCK_NONBLOCK, 0);
#endif
	if (ISERR(r))
		exit(SRV_SOCK_CREATE_FAILURE);
	srv_sock = (si)r;
	
	bool_true = 1;
	r = setsockopt(srv_sock, SOL_SOCKET, SO_REUSEADDR, &bool_true,
							sizeof(bool_true));
	if (ISERR(r))
		exit(SRV_SOCK_SET_SOCK_OPTION_FAILURE);

	bool_true = 1;
	r = setsockopt(srv_sock, SOL_SOCKET, SO_REUSEPORT, &bool_true,
							sizeof(bool_true));
	if (ISERR(r))
		exit(SRV_SOCK_SET_SOCK_OPTION_FAILURE);
	
	r = bind(srv_sock, &srv_addr, sizeof(srv_addr));
	if (ISERR(r))
		exit(SRV_SOCK_BIND_FAILURE);
	
	r = listen(srv_sock, 0);
	if (ISERR(r))
		exit(SRV_SOCK_LISTEN_FAILURE);
}

static void epoll_sigs_setup(void)
{
	struct epoll_event ep_evt;
	sl r;

	memset(&ep_evt, 0, sizeof(ep_evt));
	ep_evt.events = EPOLLET | EPOLLIN;
	ep_evt.data.fd = sigs_fd;
	r = epoll_ctl(epfd, EPOLL_CTL_ADD, sigs_fd, &ep_evt);
	if (ISERR(r))
		exit(EPOLL_SIGS_SETUP_EPOLL_ADD_FAILURE);
}

static void sigs_setup(void)
{/*
  * synchronous treatement of signals with signalfd (not a silver bullet,
  * depends on hardware for some signals)
  *
  * Cannot block SIGKILL, neither SIGSTOP.
  * 
  * SIGKILL: will terminate the process, whatever.
  * SIGSTOP: since it's not lethal, it means many blocking syscalls
  *          will return -EINTR.
  * 
  * some signals, if generated by the hardware (and not via software) are 
  * asynchronously lethal per thread by their nature, namely it cannot be
  * handled synchronously in mono-threaded processes via signalfd. It could be
  * hardware dependent (x86_64/aarch64...)
  * SIGFPE, SIGBUS, SIGSEGV, SIGILL...
  * 
  * raw signal use may disrupt glibc (pthread) operations, mix glibc
  * code with care regarding signals.
  *
  */
	u64 mask;
	sl r;

	mask = (~0);
	r = rt_sigprocmask(SIG_BLOCK, &mask, 0, sizeof(mask));
	if (ISERR(r))
		exit(SIGS_SETUP_BLOCKING_FAILURE);

	mask = SIGBIT(SIGTERM) | SIGBIT(SIGCHLD);
	sigs_fd = (si)signalfd4(-1, &mask, sizeof(mask), SFD_NONBLOCK);
	if (ISERR(sigs_fd))
		exit(SIGS_SETUP_HANDLERS_FAILURE);
}

static void setup(void)
{
	umask(0);

	sigs_setup();

	epfd =(si)epoll_create1(0);
	if (ISERR(epfd))
		exit(SETUP_EPOLL_CREATE_FAILURE);

	epoll_sigs_setup();
	srv_sock_create();
	epoll_srv_sock_setup();

	/* based on line buffer */
	forward_path_start = &cmd_resp_line[0] + FORWARD_PATH_START_IDX;

	txt_line_end = &txt_line[0] + SMTP_TXT_LINE_LEN_MAX;
}

static void globals_init(void)
{
#ifdef CONFIG_IPV4
	srv_addr.family = AF_INET;

	/* big endian port */
	srv_addr.port = cpu_to_be16(CONFIG_LISTENING_PORT);
	srv_addr.addr = cpu_to_be32(LISTENING_IPV4);
#else
	srv_addr.family = AF_INET6;

	/* big endian port */
	srv_addr.port = cpu_to_be16(CONFIG_LISTENING_PORT);
	/* all zeros means IN6ADDR_ANY */
	memset(&listening_ipv6, 0, sizeof(listening_ipv6));
	memcpy(&srv_addr.addr, &listening_ipv6, sizeof(srv_addr.addr));
#endif

	srv_sock = -1;	/* our listening socket */
	cnx_sock = -1;	/* a cnx socket from accept */

	sigs_fd = -1;	/* fd for signals */
	epfd = -1;	/* epoll fd */
}

static void chroot_do(void)
{
	sl r;

	r = chroot(CONFIG_CHROOT_PATH);
	if (ISERR(r))
		exit(CHROOT_FAILURE);

	r = chdir("/");
	if (ISERR(r))
		exit(CHDIR_FAILURE);
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/* XXX:may do the daemonic stuff/priviledge drop if _really_ we need it */
void _start(void)
{
	close(0);
	close(1);
	close(2);
	chroot_do();
	globals_init();
	setup();
	main_loop();
}
