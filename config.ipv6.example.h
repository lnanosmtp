#ifndef CONFIG_H
#define CONFIG_H
/*
 * 16 bits value for the port (below 1024, must be root, that you must be for
 * chroot anyway)
 */
#define CONFIG_LISTENING_PORT 25

/* the chroot patch used upon start */
#define CONFIG_CHROOT_PATH "/var/mail"

/* you must define the (domain/address literal) */
/* #define CONFIG_DOMAIN_ADDRESS_LITERAL "[ipv6:...]" */
#endif
