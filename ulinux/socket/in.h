#ifndef ULINUX_SOCKET_IN_H
#define ULINUX_SOCKET_IN_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
/*
 * ipv4 sockaddr must have this size, kind of minimum size for any type of
 * socketaddr
 */
#define ULINUX_SOCKADDR_IN_SZ 16 
struct ulinux_sockaddr_in {
	ulinux_u16 family;	/* AF_INET */
	ulinux_u16 port;
	ulinux_u32 addr;	/* ipv4 */

	/*pad to size of `struct sockaddr'*/
	ulinux_u8 __pad[ULINUX_SOCKADDR_IN_SZ - sizeof(ulinux_u16)
		- sizeof(ulinux_u16) - sizeof(ulinux_u32)];
};
#define ULINUX_INADDR_ANY ((ulinux_ul)0x00000000)
#endif
