#ifndef ULINUX_ARCHS_X86_64_START_S
#define ULINUX_ARCHS_X86_64_START_S
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
	.text
	.globl _start
	.type _start,@function
_start:
	xorl %ebp, %ebp /* suggested by abi */
	movq %rsp, %rdi /* stack pointer:argc, argv, envp, auxv */
	/*
	 * 32 bytes DOWN aligned stack: if already aligned, it is fine since
	 * the stack pointer is decremented before pushing/storing a value on
	 * this stack, aka we don't lose argc
	 */
	andq  $~31, %rsp
	jmp ulinux_start
#endif
