#ifndef ULINUX_ARCH_MMAP_H
#define ULINUX_ARCH_MMAP_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
/* only 4KiB base page size */
#define ULINUX_PAGE_SHIFT 12
#define ULINUX_PAGE_SZ (1 << ULINUX_PAGE_SHIFT)
#endif
