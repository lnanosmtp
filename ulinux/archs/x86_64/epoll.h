#ifndef ULINUX_ARCH_EPOLL_H
#define ULINUX_ARCH_EPOLL_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
#ifndef PACKED
	#error "missing x86_64 PACKED macro"
#endif
#define EPOLL_PACKED PACKED
#endif
