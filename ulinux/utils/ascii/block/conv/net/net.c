#ifndef ULINUX_UTILS_ASCII_BLOCK_CONV_NET_NET_C
#define ULINUX_UTILS_ASCII_BLOCK_CONV_NET_NET_C
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
/*----------------------------------------------------------------------------*/ 
#include <stdbool.h>
/*----------------------------------------------------------------------------*/ 
#include <ulinux/compiler_types.h>
#include <ulinux/types.h>
/*----------------------------------------------------------------------------*/ 
#include <ulinux/utils/mem.h>
#include <ulinux/utils/ascii/block/conv/decimal/decimal.h>
#include <ulinux/utils/ascii/block/conv/hexadecimal/hexadecimal.h>
#include <ulinux/utils/endian.h>
/*----------------------------------------------------------------------------*/ 
#include <ulinux/utils/ascii/block/conv/decimal/decimal.c>
#include <ulinux/utils/ascii/block/conv/hexadecimal/hexadecimal.c>

/*----------------------------------------------------------------------------*/
/* "One Compilation Unit" support */
#ifdef ULINUX_UTILS_EXTERNAL
#define ULINUX_EXPORT
#else
#define ULINUX_EXPORT static
#endif
/*----------------------------------------------------------------------------*/

/******************************************************************************/
/* some basic debug/trace stuff */
#if 0
#include <stdarg.h>
#include <ulinux/sysc.h>
#include <ulinux/utils/ascii/string/vsprintf.h>
#define BUFSIZ 8192
static ulinux_u8 dprintf_buf[BUFSIZ];
#define PERR(fmt,...) ulinux_dprintf(2,dprintf_buf,BUFSIZ,fmt,__VA_ARGS__)
#endif
/******************************************************************************/
/* local */
#define loop for(;;)
/*----------------------------------------------------------------------------*/ 
/*
 * strict ipv4 (xxx.xxx.xxx.xxx) decimal ascii block to 32 bits big endian
 * (network endian) ipv4
 * caller must provide a valid memory block (start <= last)
 * inplace conversion: ok because minimal address is 7 bytes to 4 bytes
 */
ULINUX_EXPORT bool ulinux_to_ipv4_blk(ulinux_u32 *dest, ulinux_u8 *start,
							ulinux_u8 *last)
{/* do *not* trust content */
	ulinux_u32 ipv4;
	ulinux_u8 *n_start;
	ulinux_u8 *n_end;
	ulinux_u8 u;
	ulinux_u8 i;

	ipv4 = 0;
	n_start = start;
	n_end = start;
	i = 0;
	loop {
		if (i > 2)
			break;

		if (*n_end == '.')
			return false;

		loop {
			if ((n_end > last) || ((n_end - n_start) > 3)
							|| (*n_end == '.'))
				break;
			++n_end;
		}

		if ((n_end == last) || (*n_end != '.'))
			return false;

		u = 0;
		if (!ulinux_dec2u8_blk(&u, n_start, n_end - 1))
			return false;
		ipv4 |= u << (24 - (i * 8)); 

		n_start = n_end + 1;
		n_end = n_start;

		++i;
	}

	u = 0;
	if (!ulinux_dec2u8_blk(&u, n_start, last))
		return false;
	ipv4 |= u;
	*dest = ulinux_cpu_to_be32(ipv4);
	return true;
}

/*
 * "good enough" conversion
 * expect a valid memory block ( start <= last )
 * there is probably a way to use malformed address for attacks
 */
#define CSTRLEN(cstr) (sizeof(cstr) - 1)
#define GROUP_IDX_MAX 7
#define u8 ulinux_u8
#define u16 ulinux_u16
#define hex_to_u16 ulinux_hex_to_u16_blk
#define cpu_to_be16 ulinux_cpu_to_be16
#define memset ulinux_memset
ULINUX_EXPORT bool ulinux_to_ipv6_blk(u8 *dest, u8 *start, u8 *last)
{/* do *not* trust content */
	u8 *c;		/* Colmun/Cursor */
	u8 *group_start;
	u8 *group_last;
	u8 group_next_idx;
	u16 *groups;
	u8 *compression_mark;
	u8 backward_group_idx_min;

	#define COMPRESSION_MARK "::"
	if ((last - start + 1) < CSTRLEN(COMPRESSION_MARK))
		return false;
	if (CSTRLEN("xxxx:xxxx:xxxx:xxxx:xxxx:xxxx:xxxx:xxxx")
							< (last - start + 1))
		return false;

	memset(dest, 0, 16);
	groups = (u16*)dest;

	/*--------------------------------------------------------------------*/
	/*
	 * forward loop, we bump on:
	 *  - last char.
	 *  - the compression mark.
	 */
	c = start;
	group_start = start;
	group_last = 0;
	group_next_idx = 0;
	compression_mark = 0;
	loop {
		if (c == last) {
			group_last = last;
		} else if (c[0] == ':') {
			/* we are not the last char, we can peek */
			if (c[1] == ':') {
				compression_mark = c;
				/* compression mark at the start */
				if (c == start)
					break; /* go to backward loop */
			}

			if (c == start) /* garbage ':' */
				return false;
			group_last = c - 1;
		}

		if (group_last != 0) {
			u16 cpu_u16;	/* cpu endian u16 or group */

			if (!hex_to_u16(&cpu_u16, group_start, group_last))
				return false;

			#define group_idx group_next_idx
			groups[group_idx] = cpu_to_be16(cpu_u16);

			if (group_last == last || group_idx == GROUP_IDX_MAX)
				if (group_last == last
						&& group_idx == GROUP_IDX_MAX)
					return true;
				else
					return false;
			#undef group_idx
			++group_next_idx;

			if (compression_mark != 0)
				break; /* go to backward loop */

			group_start = group_last + 2; /* skip the ':' */
			group_last = 0;
			c = group_start;
		} else 
			++c;
	}

	++compression_mark; /* point on the second ':' */
	if (compression_mark == last) /* compression mark an the end */
		return true;

	/* account at least 1 zero group for the compression mark */
	backward_group_idx_min = group_next_idx + 1;

	/*--------------------------------------------------------------------*/
	/*
	 * backward loop, we bump on:
	 *  - the not-at-the-end compression mark (actually its second ':').
	 *  - the minimum group idx allowed based on the compression mark
	 *    location found in the forward loop.
	 */
	c = last;
	group_start = 0;
	group_last = last;
	group_next_idx = 7;
	loop {
		if (c[0] == ':') {
			if (c == group_last) /* garbage ':' */
				return false;
			group_start = c + 1;
		}

		if (group_start != 0) {
			ulinux_u16 cpu_u16;	/* cpu endian u16 or group */
		
			if (!hex_to_u16(&cpu_u16, group_start, group_last))
				return false;

			#define group_idx group_next_idx
			groups[group_idx] = cpu_to_be16(cpu_u16);

			if ((group_start - 1) == compression_mark)
					break;

			if (group_idx == backward_group_idx_min) {
				/*
				 * maximum count of groups converted, then we
				 * must have reached the compression mark
				 */
				if ((group_start - 1) != compression_mark)
					return false;
				break;
			}
			#undef group_idx
			group_next_idx--;

			group_last =  group_start - 2; /* skip the ':' */
			group_start = 0;
			c = group_last;
		} else
			c--;
	}
	return true;
}
#undef COMPRESSION_MARK
#undef CSTRLEN
#undef GROUP_IDX_MAX 
#undef u8
#undef u16
#undef hex_to_u16
#undef cpu_to_be16
#undef memset

/*----------------------------------------------------------------------------*/ 
/* local cleanup */
#undef loop
/*----------------------------------------------------------------------------*/ 
#undef ULINUX_EXPORT
#endif
