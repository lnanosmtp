#ifndef ULINUX_COMPILER_MISC_H
#define ULINUX_COMPILER_MISC_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
/*----------------------------------------------------------------------------*/
#if __GNUC__
	#define unreachable __builtin_unreachable
	#define GCC_NOCLONE __attribute__((noclone))
	#define GCC_NOINLINE __attribute__((noinline))
#else
	#define unreachable()
#endif
/*----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------*/
#if __GNUC__
	#define PACKED __attribute__((packed))
#endif

#ifndef PACKED
	#error "the compiler must support packed structure in some way"
#endif
/*----------------------------------------------------------------------------*/
#endif
